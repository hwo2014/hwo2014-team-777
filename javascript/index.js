var net = require("net");
var JSONStream = require('JSONStream');
var _ = require('underscore');
var numeric = require('numeric');
var fs = require('fs');

// var logFileName = 'C:/Users/bnns/project/hwo-data/analysis/data/' + new Date().toISOString().replace(/:/g,"-") + '.csv'
// var log = fs.createWriteStream(logFileName, {'flags':'w'})

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

/*Global Variables*/
var carSpecs = []; //all cars on track
var endBoost = 0;
var switching = false; //have I already sent a switch message?
var switchLane = 0;
var turboStats = null;
var boost = false;
var boostPieces = [];
var myId = ""; //my car's id
var myCarLength = 0; //my car's length
var myCarRadius = 0; //car guideFlag to rear length
var myCarWidth = 0; //my car's width
var myGuideFlagDistance = 0; //pivot distance from center
var enemyStates = []; //enemy's states
var trackArray = []; //all track pieces
var trackName = ""; //track name
var trackId = ""; //id
var trackLanes = []; //lanes
var lapDistance = 0; //total distance per lap
var currentTick = 0; //current tick of game
var previousStates = null;
var currentPiece = {};
var myState = {};

//Calculated Global Variables
var k = 0; //not used kinetic constant
var myCarMass = 0; //not used guess of mass
var kinematicMatrix = []; //recorded kinematic forces
var accelerationMatrix = []; //recorded accelerations
var coefficients = []; //kinematic coefficients
var turningCoefficients = []; //cornering coefficients
var dragCoefficient = 0; //air drag
var rollingCoefficient = 0; //rolling friction
var engineCoefficient = 0; //engine power
var slipVelocityArray = []; //calculating all slip velocities
var maxSpeedArray = []; //to store max speeds for all curves
var maxRightDistance = 0; //max right distance from center lane
var maxLeftDistance = 0; //max left distance from center lane
var myInertia = 0; //moment of inertia of the car
var lastThrottle = 0.0 //last known throttle
var lastDisplayTick = 0; //last time we displayed the log
var centripetalForce = 0; //centripetal force around turns
var frictionConstant = 0; //mu of k
var centripetalCoefficient = 0; //not used
var trackCoefficient = 0; //not used
var slipCoefficient = 0; //not used
var gainConstant = 1; //for step response of curves
var omegaN = Math.sqrt(Math.E)/18; //natural frequency of system
var damping = 0.6; //guessing at the damping - related to friction
var phi = Math.PI*7/8; //offset of system

//Configuration and Timing
var sampleRate = 1; //sample every x ticks
var logRate = 20; //log every x ticks
var start = process.hrtime();

var elapsed_time = function(note){
    var precision = 3; // 3 decimal places
    var elapsed = process.hrtime(start)[1] / 1000000; // divide by a million to get nano to milli
    console.log(process.hrtime(start)[0] + " s, " + elapsed.toFixed(precision) + " ms - " + note); // print message + time
    start = process.hrtime(); // reset the timer
}

/*End Global Variables*/
//node index.js testserver.helloworldopen.com 8091 alpha zQJmSXIjLqe+fw
//node index.js senna.helloworldopen.com 8091 alpha zQJmSXIjLqe+fw
//node index.js prost.helloworldopen.com 8091 alpha zQJmSXIjLqe+fw
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
   //used for the actual race
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });

  /*return send({
    msgType: "joinRace",
    data: {
      botId: {
        name: botName,
        key: botKey
      },
    trackName: "usa", //keimola, germany, usa
    password: "dennis",
    carCount: 1
    }
  });*/

  /*return send({
    msgType: "createRace",
    data: {
      botId: {
        name: botName,
        key: botKey
      },
    trackName: "keimola", //keimola, germany, usa
    password: "dennis",
    carCount: 5 }
  });*/
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};


function calculateThrottle(myState) {
var myPiece = trackArray[myState.piecePosition.pieceIndex];
var nextPiece = trackArray[(myState.piecePosition.pieceIndex+1)%trackArray.length];
var previousPiece = trackArray[(myState.piecePosition.pieceIndex-1)%trackArray.length];
  if(currentTick > (currentTick + (endBoost - currentTick) / 2) && currentTick < endBoost){
    return .2;
  }
  if(myPiece.radius && nextPiece.length){
    return .4;
  }
  else if(myPiece.length && nextPiece.radius){
    return .4;
  }

  return 0.64;
}

function findNextSpeed(nextThrottle, currentState){
  return currentState.speed + (currentState.speed * currentState.speed * dragCoefficient)
  + (currentState.speed * rollingCoefficient) + (nextThrottle * engineCoefficient);
}

function calculateState(carPositions){


  function evaluateCurves(trackArray, myLane){
    var distance = 0;

    maxSpeedArray = [];

    for(var i = 0; i < trackArray.length; i++){
      if(trackArray[i].radius && trackArray[(i+1)%trackArray.length].length){
        //exiting curve
        distance = distBetween(i, (i+1)%trackArray.length, myLane);
        maxSpeedArray.push({ index:i, type: 'exit', maxSpeed: 0, wave: [], distanceFromLast: distance });
      }
      if(trackArray[i].radius && trackArray[(i-1)%trackArray.length].length){
        //entering curve
        if(maxSpeedArray.length)
          distance = distBetween((i-1)%trackArray.length, i, myLane);
        else
          distance = 0;
        maxSpeedArray.push({ index: i, type: 'enter', maxSpeed: 0, wave: [], distanceFromLast: distance });
      }
    }
  }

  function findLaneOffset(){
    return trackLanes[myState.piecePosition.lane.endLaneIndex].distanceFromCenter;
  }

  function calculateSlipVelocities(trackArray, currentSpeed, position, lane){
    var radius, centripetal, velocity, stepResponse, sign, nextCurve, nextSlipAmount;

    _.map(maxSpeedArray, function(piece){
      radius = trackArray[piece.index].radius + findLaneOffset();
      centripetal = currentSpeed * currentSpeed / radius;
      velocity = currentSpeed * Math.abs(Math.sin(trackArray[piece.index].angle));
      sign = (trackArray[piece.index].angle > 0) ? 1 : -1;
      addSlipPoints(sign * (velocity + centripetal), piece);
    });

    nextCurve = _.min(maxSpeedArray, function(piece){
      return (maxSpeedArray[0] === piece) ? trackArray.length - position + piece.index : piece.index - position;
    });

    nextSlipAmount = calculateNextSlip(maxSpeedArray, currentSpeed, nextCurve);

    //console.log(nextSlipAmount);

    return nextSlipAmount;
  }

  function merge(slipData){
    // console.log(slipData);
    var result = slipData.length > 0 ? slipData[0].wave : [];
    var totalLength = result.length;

    for(var i = 1; i < slipData.length; i++){
      var timeOffset = slipData[i].timeOffset;
      var slipArray = slipData[i].wave;
      var lastTimeOffset = slipData[i-1].timeOffset;
      var lastSlipArray = slipData[i-1].wave;
      for(var k = 0; k < timeOffset-lastSlipArray.length; k++){
        result.push(0);
        totalLength++;
      }
      for(var j = 0; j < slipArray.length; j++){
        if(lastSlipArray.length > (timeOffset+j)){
          result[totalLength - (lastSlipArray.length - (timeOffset + j))] += slipArray[j];
        }
        else{
          result.push(slipArray[j] || 0);
        }
      totalLength++;
      }
    }

    if(isNaN(_.reduce(result, function(memo, num){ return memo + num }, 0))){
      console.log('exiting bc result is NaN', slipData);
      process.exit(1);
    }
    return result;
  }

  function calculateNextSlip(slipArray, mySpeed, targetCurve){
    var totalSlipArray = [];
    var length = 0;
    var traversed = 0;

    _.map(slipArray, function(curve) {
      curve.timeOffset = (mySpeed > .001) ? Math.round(curve.distanceFromLast / mySpeed) : 100;
    });

    totalSlipArray = merge(slipArray);

    return totalSlipArray;
  }

  function addSlipPoints(gain, curve){
    var t = 0;
    var quantity = 0;
    do {
      t++;
      quantity = stepResponseAtTick(gain, t);
      curve.wave.push(quantity);
    }
    while(Math.abs(quantity) > 1)
  }

  function stepResponseAtTick(gain, tick){
    return ((gain * (Math.exp(-1*omegaN*damping*tick)
      * Math.sin((omegaN*Math.sqrt(1-damping*damping)*tick)+phi))) || 0);
  }

  function notInShortestPath(direction){
    return ((direction === 'Right' && currentLane.distanceFromCenter !== maxRightDistance)
      || (direction === 'Left' && currentLane.distanceFromCenter !== maxLeftDistance))
  }

  function checkLanes(enemies, direction){
    return enemies.length === _.reduce(enemies, function(memo, enemy){
      var enemyLaneIndex = enemy.piecePosition.lane.endLaneIndex;
      var myLaneIndex = currentState.piecePosition.lane.endLaneIndex;
      var distanceFromEnemy = trackLanes[enemyLaneIndex].distanceFromCenter - currentLane.distanceFromCenter;

      if(enemy.piecePosition.pieceIndex !== currentState.piecePosition.pieceIndex)
        return memo + 1;
      if(Math.abs(enemyLaneIndex - myLaneIndex) === 1
        && ((distanceFromEnemy < 0 && direction === 'Left')
          || (distanceFromEnemy > 0 && direction === 'Right')))
        return memo + 0;
      else
        return memo + 1;
    }, 0);
  }

  function distBetween(startPieceIndex, endPieceIndex, laneIndex){
		var total = 0;
		var max = (endPieceIndex - startPieceIndex) % trackArray.length;

		for(var i=0;i<max;i++){
			total += pieceLength((startPieceIndex + i) % trackArray.length, laneIndex);
		}
		return total;
	}

	function pieceLength(pieceIndex, laneIndex){
		var piece = trackArray[pieceIndex];

		if(_.has(piece, 'radius')){
			return 2 * Math.PI * (piece.radius + ((piece.angle > 0 ? -1 : 1) * trackLanes[laneIndex].distanceFromCenter))
			* Math.abs(piece.angle / 360)
		}
		return piece.length
	}

	function nextLaneChangePieceIndices(currentPieceIndex){
		var result = [];
		for(var i=0;i<trackArray.length;i++){
			if(trackArray[(i + currentPieceIndex) % trackArray.length].switch)
				result.push((i + currentPieceIndex) % trackArray.length);
		}
		return result;
	}

  function nextSlipAmount(slipVelocities,myTick){
    //console.log('ticks calculating:', slipVelocities.length);
    var total = 0;
    //console.log('tick and velocity array', myTick, slipVelocities.length);
    for(i=myTick%slipVelocities.length;i<slipVelocities.length;i++){
      if(slipVelocities[i+1] !== 0 || total === 0)
        total += slipVelocities[i];
      else
        break;
    }
    return total;
  }

	for(var i=0;i<carPositions.length;i++){
		var currentState = carPositions[i];
		currentState.slipDisplacement = 0;
		currentState.slipVelocity = 0;
    currentState.predictedSlip = 0;
		currentState.slipAcceleration = 0;
		currentState.predictedNextSpeed = 0;
		currentState.speed = 0;
		currentState.averageSpeed = 0;
		currentState.acceleration = 0;
		currentState.jerk = 0;
	}

	myState = _.find(carPositions, function(obj){ return obj.id.color === myId.color });
	var myPreviousState = _.find(previousStates, function(obj){ return obj.id.color === myId.color });
	var enemyStates = _.filter(carPositions, function(obj){ return obj.id.color !== myId.color });

	if(previousStates !== null) {

    for(var i=0;i<carPositions.length;i++){

      var currentState = carPositions[i];
			var previousState = _.find(previousStates, function(obj){ return obj.id.color === currentState.id.color });

			currentState.speed = distBetween(previousState.piecePosition.pieceIndex, currentState.piecePosition.pieceIndex, currentState.piecePosition.lane.endLaneIndex)
						- previousState.piecePosition.inPieceDistance + currentState.piecePosition.inPieceDistance

			currentState.averageSpeed = (previousState.averageSpeed * (currentTick - 1) + currentState.speed) / currentTick;

			//console.log('currentTick:', currentTick, 'avgSpeed:', currentState.averageSpeed, 'speed:', currentState.speed);

			//currentState.speed *= 60

			currentState.acceleration = currentState.speed - previousState.speed
			currentState.jerk = currentState.acceleration - previousState.acceleration

			currentState['slipDisplacement'] =  currentState.angle - previousState.angle;
			currentState['slipVelocity'] = currentState.slipDisplacement * 60;
			currentState['slipAcceleration'] = currentState.slipVelocity - previousState.slipVelocity;
		}

    //evaluateCurves(trackArray);
    //myState['predictedSlip'] = calculateSlipVelocities(trackArray, currentState.speed,
      //currentState.piecePosition.pieceIndex, myState.piecePosition.lane.endLaneIndex);

		//todo: boost location
		boost = (turboStats != null && _.contains(boostPieces, myState.piecePosition.pieceIndex));
		/*if(boost || myPreviousState.piecePosition.pieceIndex != myState.piecePosition.pieceIndex)
			console.log('boost:', boost, 'turboStat', turboStats)*/

		var nextPiece = trackArray[(myState.piecePosition.pieceIndex + 1) % trackArray.length]
		var laneChangePieces = nextPiece['switch'] ? nextLaneChangePieceIndices(myState.piecePosition.pieceIndex):[]
		if(!nextPiece['switch']){
			switching = false;
			switchLane = 0;
		}

		if(laneChangePieces.length != 0){
			//calculate shortest path between 2 switch pieces
			var nextLaneChangePieceDistance = []
			for(var i=0;i<trackLanes.length;i++){
				if(laneChangePieces.length == 1)
					nextLaneChangePieceDistance.push(distBetween(laneChangePieces[0], (laneChangePieces[0] - 1) % trackArray.length, i)
					+ pieceLength((laneChangePieces[0] - 1) % trackArray.length, i))
				else
					nextLaneChangePieceDistance.push(distBetween(laneChangePieces[0], laneChangePieces[1], i))
			}

			//minimum distance to collision on each lane
			var minLaneCollisionDistance = _.map(trackLanes, function(obj){ return Number.MAX_VALUE });
			for(var i=0;i<enemyStates.length;i++){
				var enemyState = enemyStates[i];
				var dSpeed = myState.speed - enemyState.speed;//todo: not sure if should use avg speed
				var dDistance = distBetween(myState.piecePosition.pieceIndex,
					enemyState.piecePosition.pieceIndex, enemyState.piecePosition.lane.endLaneIndex)
					+ enemyState.piecePosition.inPieceDistance - myState.piecePosition.inPieceDistance;
				var timeToCollision = dDistance / dSpeed;
				var distanceToCollision = myState.speed * timeToCollision;
				if(distanceToCollision < minLaneCollisionDistance[enemyState.piecePosition.lane.endLaneIndex])
					minLaneCollisionDistance[enemyState.piecePosition.lane.endLaneIndex] = distanceToCollision
			}
			//checks if lane change can be done on next chance
			for(var i=0;i<minLaneCollisionDistance.length;i++)
				minLaneCollisionDistance[i] -= nextLaneChangePieceDistance[i];

			var endLaneIndex = myState.piecePosition.lane.endLaneIndex;
			switchLane = 0;

			//will switch into lane with shortest path (-1 = left, 0 = none, 1 = right)
			var endLaneIndex = myState.piecePosition.lane.endLaneIndex;
			switchLane = 0;
			if(endLaneIndex - 1 >= 0 && nextLaneChangePieceDistance[endLaneIndex - 1] < nextLaneChangePieceDistance[endLaneIndex])
				switchLane = -1;
			else if(endLaneIndex + 1 < trackLanes.length && nextLaneChangePieceDistance[endLaneIndex + 1] < nextLaneChangePieceDistance[endLaneIndex])
				switchLane = 1;

			if(switchLane == 1 && endLaneIndex - 1 >= 0 && nextLaneChangePieceDistance[endLaneIndex - 1] < nextLaneChangePieceDistance[endLaneIndex + 1])
				switchLane = -1;
			else if(switchLane == -1 && endLaneIndex + 1 < trackLanes.length && nextLaneChangePieceDistance[endLaneIndex + 1] < nextLaneChangePieceDistance[endLaneIndex - 1])
				switchLane = 1;

			//will switch into lane with least likelyhood of collision (-1 = left, 0 = none, 1 = right)
			if(endLaneIndex - 1 >= 0 && minLaneCollisionDistance[endLaneIndex - 1] > minLaneCollisionDistance[endLaneIndex] && minLaneCollisionDistance[endLaneIndex+switchLane] < 0)
				switchLane = -1;
			else if(endLaneIndex + 1 < trackLanes.length && minLaneCollisionDistance[endLaneIndex + 1] > minLaneCollisionDistance[endLaneIndex] && minLaneCollisionDistance[endLaneIndex+switchLane] < 0)
				switchLane = 1;

			if(switchLane == 1 && endLaneIndex - 1 >= 0 && minLaneCollisionDistance[endLaneIndex - 1] > minLaneCollisionDistance[endLaneIndex + 1] && minLaneCollisionDistance[endLaneIndex+switchLane] < 0)
				switchLane = -1;
			else if(switchLane == -1 && endLaneIndex + 1 < trackLanes.length && minLaneCollisionDistance[endLaneIndex + 1] > minLaneCollisionDistance[endLaneIndex - 1] && minLaneCollisionDistance[endLaneIndex-switchLane] < 0)
				switchLane = 1;

			//console.log('collision', minLaneCollisionDistance, 'shortest', nextLaneChangePieceDistance, 'switch', switchLane);
		}


		if(myState.speed > 0 && myState.acceleration > 0 && lastThrottle > 0 && kinematicMatrix.length < 3){
			kinematicMatrix.push([myState.speed*myState.speed, myState.speed, lastThrottle]);
			accelerationMatrix.push(myState.acceleration);
		}
		if(kinematicMatrix.length === 3 && accelerationMatrix.length === 3 && coefficients.length === 0){
			coefficients = numeric.solve(kinematicMatrix, accelerationMatrix);
			//console.log('estimated kinematic coefficients: ', coefficients);
			dragCoefficient = coefficients[0];
			rollingCoefficient = coefficients[1];
			engineCoefficient = coefficients[2];
		}
	}

	if(coefficients.length === 3)
		myState['predictedNextSpeed'] = findNextSpeed(0.6, myState);

  logGameState('standard', myState, myPreviousState);
  return carPositions;
}

function appendFile(entry){
  log.write(entry);
  log.write('\r\n');
};

function logGameState(message, currentState, lastState){

  if(currentTick >= lastDisplayTick + logRate){

    lastDisplayTick = currentTick;
  };

  //appendFile(outString);
};

function boost(){
  return false;
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
	//console.log('tick:',data.gameTick);
	if (data.msgType === 'carPositions') {
		//elapsed_time('message');
		currentTick = data.gameTick;
		//elapsed_time('start '+currentTick);
		previousStates = calculateState(data.data);

		if(!switching && switchLane != 0){
		  switching = true;
		  send({"msgType": "switchLane", "data": switchLane == -1 ? "Left":"Right" });
		}
		else if (boost){
      boost = false;
      endBoost = turboStats.turboDurationTicks + currentTick;
			turboStats = null;
			console.log('TO INFINITY AND BEYOND!!');
		  send({"msgType": "turbo", "data": "!!!"});
		}
		else{
		  nextThrottle = calculateThrottle(myState);
		  lastThrottle = nextThrottle;
		  //elapsed_time('throttle');
		  send({
			msgType: "throttle",
			data: nextThrottle/*,
			gameTick: currentTick*/
		  });
		}
		//elapsed_time('stop');
	} else if (data.msgType === 'join') {
      console.log('Joined')
	} else if (data.msgType === 'turboAvailable') {
      console.log('Turbo Available')
	  turboStats = data.data
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');

      var thisCar = _.find(carSpecs, function(obj) {return obj.id.color === myId.color});

      myGuideFlagDistance = thisCar.dimensions.guideFlagPosition;
      myCarLength = thisCar.dimensions.length;
      myCarWidth = thisCar.dimensions.width;
      myInertia = ((myCarLength * myCarLength) + (myCarWidth * myCarWidth)) / 12;
      myCarRadius = myGuideFlagDistance + (myCarLength / 2);

    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'gameInit') {

      carSpecs = data.data.race.cars;
      trackArray = data.data.race.track.pieces;
      trackName = data.data.race.track.name;
      trackId = data.data.race.track.Id;
      trackLanes = data.data.race.track.lanes;

      //best boost pieces
	  var longestStraightWay = 0;
	  for(var i=0;i<trackArray.length;i++){
		var j=0;
		while(!_.has(trackArray[(i+j) % trackArray.length], 'radius')){
			j++;
		}
		if(j > longestStraightWay){
			boostPieces = [i]
			longestStraightWay = j;
		}else if(j == longestStraightWay){
			boostPieces.push(i);
		}
	  }
	  //console.log('boostPieces', boostPieces);

      console.log('Initialized race parameters');
    } else if (data.msgType === 'yourCar') {
      myId = data.data;
    }

    send({
       msgType: "ping",
       data: {}
    });
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
